package main

import (
	"fmt"
	"time"
	"encoding/json"
	"context"
	"math"
	"errors"
	"sync"
	"github.com/influxdata/influxdb-client-go/v2"
)

type Collector interface {
	Collect(*sync.WaitGroup, chan int, chan error)
}

type OhlcCollector struct {
	Logging *Loggers
	CollectionWaitDuration time.Duration
	MaxApiWaitDuration time.Duration
	Client influxdb2.Client
	Org string
	Bucket string
	Symbol string
	ApiKey string
}

func NewOhlcCollector(
		loggers *Loggers,
		initWaitDuration time.Duration,
		maxApiWaitDuration time.Duration,
		dbclient influxdb2.Client,
		org string,
		bucket string,
		symbol string,
		apikey string,
) *OhlcCollector {
	return &OhlcCollector{loggers, initWaitDuration, maxApiWaitDuration, dbclient, org, bucket, symbol, apikey}
}

func (c *OhlcCollector) GetCollectionDates() ([]time.Time, error) {
	query := fmt.Sprintf("from(bucket:\"%s\") |> range(start: 0) |> filter(fn: (r) => r[\"_measurement\"] == \"OHLC\" and r[\"symbol\"] == \"%s\" and r[\"_field\"] == \"open\") |> keep(columns: [\"_time\", \"_value\"]) |> sort(columns: [\"_time\", \"_value\"], desc: false) |> last() |> keep(columns: [\"_time\"])",
		c.Bucket, c.Symbol)
	queryAPI := c.Client.QueryAPI(c.Org)
	result, err := queryAPI.Query(context.Background(), query)

	var most_rec_time time.Time
	if err == nil {
		for result.Next() {
			most_rec_time = result.Record().Time()
		}
	} else {
		return nil, err
	}

	cur_time := time.Now()
	cur_time = cur_time.AddDate(0,0,-1)
	var dur time.Duration
	var min_date time.Time
	utc_log, _ := time.LoadLocation("UTC")
	min_time := time.Date(1,1,1,0,0,0,0,utc_log)
	if most_rec_time == min_time {
		min_date = cur_time.AddDate(-1,-11,-14)
		dur = cur_time.Sub(min_date)
		c.Logging.InfoLogger.Printf("%s OHLC collection start date = %v\n", c.Symbol, min_date)
	} else {
		date_to_use := most_rec_time.AddDate(0,0,1)
		c.Logging.InfoLogger.Printf("%s OHLC collection start date = %v\n", c.Symbol, date_to_use)
		dur = cur_time.Sub(date_to_use)
	}
	c.Logging.InfoLogger.Printf("%s OHLC collection end date = %v\n", c.Symbol, cur_time)

	num_days := int(math.Ceil(dur.Hours() / 24))
	dates := make([]time.Time, num_days)
	for i := 0; i < num_days; i++ {
		dates[i] = cur_time.AddDate(0,0, -num_days+i+1)
	}

	return dates, nil
}

func (c *OhlcCollector) CanCollectData() (bool, error) {
	dates, err := c.GetCollectionDates()
	if err != nil {
		return false, err
	}

	return len(dates) > 0, nil
}

func (c *OhlcCollector) Collect(wg *sync.WaitGroup, numCollChangec chan int, errsig chan error) {
	defer wg.Done()
	defer c.Logging.InfoLogger.Printf("OHLC data collection ending for %s\n", c.Symbol)

	//get ohlc data from polygon.io
	dates, err := c.GetCollectionDates()
	if err != nil {
		errsig <- err
		numCollChangec <- -1
		return
	}
	
	if len(dates) == 0 {
		c.Logging.InfoLogger.Printf("No new OHLC data to collect for %s\n", c.Symbol)
		numCollChangec <- -1
		return
	}

	go func() {
		for diff := range numCollChangec {
			c.CollectionWaitDuration += c.MaxApiWaitDuration * time.Duration(diff)
		}
	}()

	httpclient := c.Client.Options().HTTPClient()
	for _, date := range dates {
		year, monstr, day := date.Date()
		mon := int(monstr)
		datestr := fmt.Sprintf("%d-%02d-%02d", year, mon, day)
		c.Logging.InfoLogger.Printf("%s OHLC data date: %s\n", c.Symbol, datestr)
		requrl := fmt.Sprintf("https://api.polygon.io/v1/open-close/%s/%s?adjusted=true&apiKey=%s", c.Symbol, datestr, c.ApiKey)
		resp, err := httpclient.Get(requrl)
		if err != nil {
			errsig <- err
			numCollChangec <- -1
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode == 400 || resp.StatusCode == 404 {
			c.Logging.InfoLogger.Printf("Skipping data collection due to response status %d.\n", resp.StatusCode)
			time.Sleep(c.CollectionWaitDuration)
			continue
		}
	
		ohlc := &OhlcData{}
		err = json.NewDecoder(resp.Body).Decode(ohlc)
		if err != nil {
			errsig <- err
			numCollChangec <- -1
			return
		}
	
		data := map[string]interface{}{
			"open": ohlc.Open,
			"high": ohlc.High,
			"low": ohlc.Low,
			"close": ohlc.Close,
			"volume": ohlc.Volume,
			"pre_market": ohlc.PreMarket,
			"post_market": ohlc.AfterHours}
		
		writeAPI := c.Client.WriteAPI(c.Org, c.Bucket)
		//write errors to log
		errout := writeAPI.Errors()
		go func() {
			for err := range errout {
				if err != nil {
					errsig <- errors.New(fmt.Sprintf("%s OHLC data write error: %v", c.Symbol, err.Error()))
				}
			}
		}()
		//write to db
		p := influxdb2.NewPoint("OHLC", map[string]string{"symbol": c.Symbol}, data, date)
		writeAPI.WritePoint(p)
		writeAPI.Flush()
	
		for key, val := range data {
			c.Logging.InfoLogger.Printf("%s OHLC wrote %s = %v\n", c.Symbol, key, val)
		}
		time.Sleep(c.CollectionWaitDuration)
	}
	numCollChangec <- -1
}

type CollectorCoordinator interface {
	CollectAll(chan error)
}

type OhlcCollectorCoordinator struct {
	Collectors []*OhlcCollector
	MaxApiWaitDuration time.Duration
}

func NewOhlcCollectorCoordinator(collectors []*OhlcCollector, maxApiWaitDuration time.Duration) *OhlcCollectorCoordinator {
	return &OhlcCollectorCoordinator{collectors, maxApiWaitDuration}
}

func (c *OhlcCollectorCoordinator) InitializeWaitDurations(collsAbleToCollect []*OhlcCollector) {
	initNumCollectors := len(collsAbleToCollect)
	for i := 0; i < initNumCollectors; i++ {
		collsAbleToCollect[i].CollectionWaitDuration = time.Duration(initNumCollectors) * c.MaxApiWaitDuration
	}
}

func (c *OhlcCollectorCoordinator) GetCollectorsAbleToCollectData(errc chan error) []*OhlcCollector {
	var canCollectors []*OhlcCollector
	for i := 0; i < len(c.Collectors); i++ {
		canCollect, err := c.Collectors[i].CanCollectData()
		if err != nil {
			errc <- err
		}
		if canCollect {
			canCollectors = append(canCollectors, c.Collectors[i])
		}
	}
	return canCollectors
}

func (c *OhlcCollectorCoordinator) CollectAll(errc chan error) {
	collectors := c.GetCollectorsAbleToCollectData(errc)
	c.InitializeWaitDurations(collectors)

	numCollChangec := make(chan int)
	var wg sync.WaitGroup
	for i := 0; i < len(collectors); i++ {
		wg.Add(1)
		go collectors[i].Collect(&wg, numCollChangec, errc)
		time.Sleep(c.MaxApiWaitDuration)
	}
	wg.Wait()
	close(numCollChangec)
	close(errc)
}