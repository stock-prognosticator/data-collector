package main

import (
	"log"
	"os"
)

type Loggers struct {
	InfoLogger *log.Logger
	ErrLogger *log.Logger
}

func NewLoggers() *Loggers {
	InfoLogger := log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrLogger := log.New(os.Stdout, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	InfoLogger.Println("Starting log.")
	loggers := Loggers{InfoLogger, ErrLogger}
	return &loggers
}