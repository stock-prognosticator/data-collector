package main

type OhlcData struct {
	Open float64
	High float64
	Low float64
	Close float64
	Volume float64
	PreMarket float64
	AfterHours float64
	//unused properties, but come from the polygon.io api
	From string
	Status string
	Symbol string
}