package main

import (
	"os"
	"time"
	"encoding/json"
	"github.com/influxdata/influxdb-client-go/v2"
)

type RawConfig struct {
	DbAuthToken string
	DbEndpoint string
	PolygonAuthToken string
	PolygonApiTimeout int
	CollectionDuration int
	DbBucket string
	DbOrg string
	Symbols []string
}

type Config struct {
	DbAuthToken string
	DbEndpoint string
	PolygonAuthToken string
	PolygonApiTimeout time.Duration
	CollectionDuration time.Duration
	DbBucket string
	DbOrg string
	Symbols []string
}

func (c RawConfig) ToConfig() Config {
	return Config{
		c.DbAuthToken,
		c.DbEndpoint,
		c.PolygonAuthToken,
		time.Duration(c.PolygonApiTimeout) * time.Second,
		time.Duration(c.CollectionDuration) * time.Hour,
		c.DbBucket,
		c.DbOrg,
		c.Symbols,
	}
}

func main() {
	jFile, err := os.Open("config.json")
	if err != nil {
		panic(err)
	}
	defer jFile.Close()
	rawconfig := &RawConfig{}
	err = json.NewDecoder(jFile).Decode(rawconfig)
	if err != nil {
		panic(err)
	}
	config := rawconfig.ToConfig()

	loggers := NewLoggers()
	dbClient := influxdb2.NewClient(config.DbEndpoint, config.DbAuthToken)

	ohlcCollectors := make([]*OhlcCollector, len(config.Symbols))
	for i := 0; i < len(config.Symbols); i++ {
		ohlcCollectors[i] = NewOhlcCollector(
			loggers,
			config.PolygonApiTimeout,
			config.PolygonApiTimeout,
			dbClient,
			config.DbOrg,
			config.DbBucket,
			config.Symbols[i],
			config.PolygonAuthToken,
		)
	}
	ohlcCoord := NewOhlcCollectorCoordinator(ohlcCollectors, config.PolygonApiTimeout)

	for {
		loggers.InfoLogger.Printf("Data collection beginning.")
		errc := make(chan error)
		ohlcCoord.CollectAll(errc)
		for err := range errc {
			if err != nil {
				loggers.ErrLogger.Printf("%v", err)
			}
		}
		loggers.InfoLogger.Printf("Data collection ending.")
		time.Sleep(config.CollectionDuration)
	}

	defer dbClient.Close()
}