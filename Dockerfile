FROM golang:1.17.0-alpine3.14

WORKDIR /app

COPY ./go.mod ./
COPY ./go.sum ./
RUN go mod download

COPY ./data-collector/*.go ./

RUN go build -o /data-collector

CMD [ "/data-collector" ]
